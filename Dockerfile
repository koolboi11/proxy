FROM registry.gitlab.com/openstapps/projectmanagement/node

USER root

RUN apk update && \
    apk upgrade && \
    apk add openssl && \
    apk add nginx && \
    apk add nginx-mod-http-vts && \
    rm -rf /var/cache/apk/*

ADD . /app

WORKDIR /app

RUN mv /app/nginx.conf /etc/nginx/nginx.conf

CMD ["sh", "./bin/run-docker.sh"]
